﻿namespace SimpleReporting
{
    partial class FrmChooseDb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewConns = new System.Windows.Forms.DataGridView();
            this.ColName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColConnString = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewConns)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewConns
            // 
            this.dataGridViewConns.AllowUserToAddRows = false;
            this.dataGridViewConns.AllowUserToDeleteRows = false;
            this.dataGridViewConns.AllowUserToOrderColumns = true;
            this.dataGridViewConns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewConns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewConns.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColName,
            this.ColConnString});
            this.dataGridViewConns.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewConns.Name = "dataGridViewConns";
            this.dataGridViewConns.ReadOnly = true;
            this.dataGridViewConns.Size = new System.Drawing.Size(408, 137);
            this.dataGridViewConns.TabIndex = 0;
            this.dataGridViewConns.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewConns_CellDoubleClick);
            // 
            // ColName
            // 
            this.ColName.HeaderText = "Name";
            this.ColName.Name = "ColName";
            this.ColName.ReadOnly = true;
            // 
            // ColConnString
            // 
            this.ColConnString.HeaderText = "Connection String";
            this.ColConnString.Name = "ColConnString";
            this.ColConnString.ReadOnly = true;
            // 
            // FrmChooseDb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 161);
            this.Controls.Add(this.dataGridViewConns);
            this.Name = "FrmChooseDb";
            this.Text = "Choose Database";
            this.Load += new System.EventHandler(this.FrmChooseDb_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewConns)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewConns;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColConnString;

    }
}