﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace SimpleReporting
{
    public partial class FrmChooseDb : Form
    {
        public FrmChooseDb()
        {
            InitializeComponent();
        }

        private void FrmChooseDb_Load(object sender, EventArgs e)
        {
            if (File.Exists("dbs.txt"))
            {
                string[] lines = File.ReadAllLines("dbs.txt");
                for (int i = 0; i < lines.Length; i++)
                {
                    string[] tmp = lines[i].Split(new char[] { ':' }, 2);
                    if (tmp.Length > 1)
                    {
                        if (lines.Length == 1)
                        {
                            // don't do the rest if there is only one
                            doCheck(tmp[1], tmp[0]);
                        }
                        dataGridViewConns.Rows.Add(
                            tmp[0],
                            tmp[1]
                        );
                    }
                }
                lines.ToList().ForEach(x => {
                    
                });
            }
            else
            {
                dataGridViewConns.Rows.Add("Config File Not Found", "");
            }
        }

        private void dataGridViewConns_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string connStr = dataGridViewConns[1, e.RowIndex].Value.ToString();
            string connName = dataGridViewConns[0, e.RowIndex].Value.ToString();
            doCheck(connStr, connName);
        }

        protected void doCheck(string connStr, string connName)
        {
            if (connStr != "")
            {
                // test it FIRST
                try
                {
                    OleDbConnection conn = new OleDbConnection(connStr);
                    conn.Open();
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Connection To Database Failed");
                    return;
                }
                Statics.ConnName = connName;
                Statics.ConnStr = connStr;
            }
            this.Close();
        }
    }
}
