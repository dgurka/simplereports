﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleReporting
{
    public partial class FrmParamText : Form
    {
        protected string _text = null;
        public string Value { get; set; }

        public FrmParamText(string text)
        {
            InitializeComponent();
            _text = text;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            this.Value = textBoxData.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void FrmParamText_Load(object sender, EventArgs e)
        {
            label1.Text = _text + ":";
            this.Text = _text;
        }

        private void textBoxData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                buttonSave_Click(sender, e);
            }
        }
    }
}
