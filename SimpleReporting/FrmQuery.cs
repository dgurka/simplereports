﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;
using System.Text.RegularExpressions;

namespace SimpleReporting
{
    public partial class FrmQuery : Form
    {
        private string _query = null;
        public FrmQuery()
        {
            InitializeComponent();
        }

        public FrmQuery(string file)
        {
            InitializeComponent();
            this.Text = file;
            this._query = File.ReadAllText(file);
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            List<Object> parameters = new List<object>();
            MatchCollection matches = Regex.Matches(textBoxQuery.Text, "-- Param:([^:]+):(Text|Date)");
            foreach (Match match in matches)
            {
                FrmParamText t = new FrmParamText(match.Groups[1].Value);
                if (t.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    return;

                parameters.Add(t.Value);
            }

            Match ckBack = Regex.Match(textBoxQuery.Text, "-- CheckBackup");

            if (ckBack.Value != "")
            {
                if (MessageBox.Show("Did you run backup?", "Did you run backup?", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }
            }

            try
            {
                OleDbConnection conn = new OleDbConnection(Statics.ConnStr);
                conn.Open();

                OleDbCommand cmd = new OleDbCommand(textBoxQuery.Text, conn);

                for (int i = 0; i < parameters.Count; i++)
                {
                    cmd.Parameters.AddWithValue(i.ToString(), parameters[i]);
                }

                OleDbDataReader reader = cmd.ExecuteReader();

                List<ColumnData> cList = new List<ColumnData>();

                if (reader.HasRows)
                {
                    DataTable tmp = reader.GetSchemaTable();
                    foreach (DataRow field in tmp.Rows)
                    {
                        ColumnData cData = new ColumnData();
                        foreach (DataColumn attrib in tmp.Columns)
                        {
                            if (attrib.ColumnName == "ColumnName")
                            {
                                cData.Name = field[attrib].ToString();
                            }
                            else if (attrib.ColumnName == "DataType")
                            {
                                cData.DataType = field[attrib].ToString();
                            }
                        }
                        cList.Add(cData);
                    }

                    // got the list
                    cList.ForEach(x =>
                    {
                        DataGridViewColumn c = new DataGridViewTextBoxColumn();
                        c.HeaderText = x.Name;
                        dataGridView1.Columns.Add(c);
                    });

                    while (reader.Read())
                    {
                        List<Object> rData = new List<object>();
                        int i = 0;
                        cList.ForEach(x =>
                        {
                            if (reader[i] == DBNull.Value)
                                rData.Add("NULL");
                            else
                            rData.Add(reader[i].ToString());
                            i++;
                        });
                        dataGridView1.Rows.Add(rData.ToArray());
                    }
                }

                if (reader.RecordsAffected >= 0)
                {
                    MessageBox.Show(String.Format("{0} Record(s) affected", reader.RecordsAffected));
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (this.Text == "New Query")
            {
                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    File.WriteAllText(saveFileDialog1.FileName, textBoxQuery.Text);
                    MessageBox.Show("Save Complete");
                    this.Text = saveFileDialog1.FileName;
                }
            }
            else
            {
                File.WriteAllText(this.Text, textBoxQuery.Text);
                MessageBox.Show("Save Complete");
            }
        }

        private void FrmQuery_Load(object sender, EventArgs e)
        {
            if (this._query != null)
                this.textBoxQuery.Text = this._query;
        }

        private void buttonSaveAs_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog1.FileName, textBoxQuery.Text);
                MessageBox.Show("Save Complete");
                this.Text = saveFileDialog1.FileName;
            }
        }

        private void textBoxQuery_DragDrop(object sender, DragEventArgs e)
        {
            MessageBox.Show(e.Data.GetData(typeof(string)).ToString());
        }

        private void textBoxQuery_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }
    }
}
