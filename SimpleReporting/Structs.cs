﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleReporting
{
    public struct ColumnData
    {
        public string Name;
        public string DataType;
    }
}
